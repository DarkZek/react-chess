import GameTile from './Chess.js';
import DetectMoves from "./AIMoveDetection.js";

class ChessAI {

    constructor() {
        this.cloneDeep = require('lodash.clonedeep');
    }

    Think(board) {

        var pieces = this.GetPieces(board);

        //Get possible moves
        var moves = this.GetMoves(pieces, board);

        if (moves == 0) {
            return null;
        }

        var move = moves[Math.floor((Math.random() * moves.length))];

        return move;
    }

    ReverseMove(move) {
        move.pieceLocationRow = 8 - move.pieceLocationRow;

        move.pieceMoveLocationRow = 8 - move.pieceMoveLocationRow;

        return move;
    }

    ReverseBoard(board) {
        var newBoard = board;

        for(var x = 8; x > 0; x--) {
            for(var y = 1; y < 9; y++) {
                newBoard[9 - x][y] = board[x][y];
            }
        }

        return newBoard;
    }

    GetPieces(board) {

        var pieces = [];

        for(var x = 1; x < 9; x++) {
            for(var y = 1; y < 9; y++) {
                if (board[x][y].owner == 2) {
                    pieces.push(board[x][y]);
                }
            }
        }

        return pieces;
    }

    GetMoves(pieces, board) {

        var moves = [];

        for(var i = 0; i < pieces.length; i++) {

            var piece = pieces[i];

            //Get moves
            var possibleMoves = DetectMoves(piece.row, piece.column, piece.piece, board, 1);

            //Add to list
            for(var move = 0; move < possibleMoves.length; move++) {

                var moveLocation = possibleMoves[move];

                moves.push(new AIMove(piece.row, piece.column, moveLocation.charAt(0), moveLocation.charAt(1)));
            }
        }

        return moves;
    }
}

class AIMove {
    constructor(pieceLocationRow, pieceLocationColumn, pieceMoveLocationRow, pieceMoveLocationColumn) {
        this.pieceLocationRow = pieceLocationRow;
        this.pieceLocationColumn = pieceLocationColumn;
        this.pieceMoveLocationRow = pieceMoveLocationRow;
        this.pieceMoveLocationColumn = pieceMoveLocationColumn;
    }
}

export default ChessAI;
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import DetectMoves from "./MoveDetection.js";
import ChessAI from "./ChessAI.js";
import PieceDetection from "./PieceDetection.js";

class Board extends React.Component {

    constructor(props) {
        super(props)
        var chess = require("node-chess");

        this.state = {
            board: new Array(8),
            selectedRow: 0,
            selectedColumn: 0,
            chessAI: new ChessAI(),
            isPlayerTurn: true,
        };

        this.generateBoard();
    }

    renderSquare(tile) {
        return <Square selected={tile.selected} number={tile.row + "" + tile.column} 
            piece={tile.piece} owner={tile.owner} 
            onClick={() => this.handleClick(tile.row, tile.column, tile.piece)} />;
    }

    render() {
        return (
        <div>
            {this.displayBoard()}
        </div>
        );
    }

    generateBoard() {
        var board = new Array(8);

        for(var row = 1; row < 9; row++) {
            board[row] = new Array(8);
            for(var column = 1; column < 9; column++) {
                board[row][column] = new GameTile(row, column, "");
            }
        }

        //Rooks
        board[1][1].piece = "R";
        board[8][1].piece = "R";
        board[1][8].piece = "R";
        board[8][8].piece = "R";

        //Knights
        board[1][2].piece = "k";
        board[8][2].piece = "k";
        board[1][7].piece = "k";
        board[8][7].piece = "k";

        //Bishop
        board[1][3].piece = "B";
        board[8][3].piece = "B";
        board[1][6].piece = "B";
        board[8][6].piece = "B";

        //Queen
        board[1][4].piece = "Q";
        board[8][4].piece = "Q";

        //King
        board[1][5].piece = "K";
        board[8][5].piece = "K";

        //Pawns
        board[2][1].piece = "P";
        board[2][2].piece = "P";
        board[2][3].piece = "P";
        board[2][4].piece = "P";
        board[2][5].piece = "P";
        board[2][6].piece = "P";
        board[2][7].piece = "P";
        board[2][8].piece = "P";
        board[7][1].piece = "P";
        board[7][2].piece = "P";
        board[7][3].piece = "P";
        board[7][4].piece = "P";
        board[7][5].piece = "P";
        board[7][6].piece = "P";
        board[7][7].piece = "P";
        board[7][8].piece = "P";

        //Set piece colors
        for(var row = 1; row < 3; row++) {
            for(var column = 1; column < 9; column++) {
                board[row][column].owner = 2;
            }
        }

        for(var row = 7; row < 9; row++) {
            for(var column = 1; column < 9; column++) {
                board[row][column].owner = 1;
            }
        }

        this.state.board = board;
    }

    displayBoard() {

        const elements = []
        const board = this.state.board;

        //Check queen promotions & everything
        if (PieceDetection(board)) {
            //Game over!
            this.state.isPlayerTurn = false;
        }

        for(var row = 1; row < 9; row++) {

            const columnElements = []

            for(var column = 1; column < 9; column++) {
                columnElements.push(this.renderSquare(board[row][column]));
            }

            elements.push(<div className="board-row">{columnElements}</div>);
        }

        return elements;
    }

    //Show moves
    handleClick(row, column, piece) {

        if (!this.state.isPlayerTurn) {
            return;
        }

        var board = this.state.board;

        //If clicking tile thats selected to be able to move
        if (board[row][column].selected) {
            const selectedRow = this.state.selectedRow;
            const selectedColumn = this.state.selectedColumn;

            //Set piece
            board[row][column].piece = board[selectedRow][selectedColumn].piece;

            //Set color
            board[row][column].owner = board[selectedRow][selectedColumn].owner;

            //Remove old piece
            board[selectedRow][selectedColumn].piece = "";
            board[selectedRow][selectedColumn].owner = 0;

            this.resetSelected();

            this.state.isPlayerTurn = false;

            var timeBefore = Date.now();
            var move = this.state.chessAI.Think(board);
            console.log("Took " + (Date.now() - timeBefore) + " ms to calculate move");

            if (move == null) {
                this.forceUpdate();
                alert("Stale mate");
                return;
            }

            //Process move

            //Get piece
            var piece = board[move.pieceLocationRow][move.pieceLocationColumn];
            var targetPiece = board[move.pieceMoveLocationRow][move.pieceMoveLocationColumn];
            targetPiece.piece = piece.piece;
            targetPiece.owner = piece.owner;

            //Remove old piece
            piece.piece = "";
            piece.owner = 0;

            this.state.isPlayerTurn = true;

        //If user owns tile
        } else if (board[row][column].owner == 1) {
            this.resetSelected();

            board = this.state.board;

            //Get moves
            DetectMoves(row, column, piece, board, 2).forEach(function(move) {
                //Show moves
                board[move.charAt(0)][move.charAt(1)].selected = true;
            });

            this.state.selectedRow = row;
            this.state.selectedColumn = column;
        } 

        this.forceUpdate();
    }

    resetSelected() {
        const board = this.state.board;

        for(var x = 1; x < 9; x++) {
            for(var y = 1; y < 9; y++) {
                board[x][y].selected = false;
            }
        }

        this.state.board = board;
    }
}

class GameTile {
    constructor(row, column, piece, isWhite) {
        this.row = row;
        this.column = column;
        this.piece = piece;
        this.selected = false;
        this.owner = 0;
    }
}

class Square extends React.Component {
    render() {
        const squareColor = this.getColor(this.props.number, this.props.selected);
        var image = "";

        if (this.props.piece != "") {
            image = <img owner={this.props.owner} src={"/pieces/" + this.props.piece + ".svg"}/>;
        }

        return (
            <button className={squareColor + " square"}
                onClick={() => this.props.onClick()}>
                {image}
            </button>
        );
    }

    getColor(position, selected) {
        if (selected) {
            return "selected";
        }
        //Convert to string
        position = position + "";
        var row = parseInt(position.charAt(0));
        var column = parseInt(position.charAt(1));

        if (row % 2 === 0) {
            //Offset column by 1
            column += 1;
        }

        if (column % 2 === 0) {
            return "colored";
        }

        return "";
    }
}

export default Board;
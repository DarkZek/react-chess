//This script provides functionality to detect special chess moves (castling, checks, queen promotion and checkmates)

function PieceDetection(board) {
    if (checkWin(board)) {
        return true;
    }

    checkQueenPromotion(board);

    return false;
}

function checkQueenPromotion(board) {
    //Check white
    for(var col = 1; col < 9; col++) {
        var piece = board[1][col];

        if (piece.piece == "P") {
            //Promote!
            promoteQueen(piece);
            return;
        }
    }

    //Check black
    for(var col = 1; col < 9; col++) {
        var piece = board[8][col];

        if (piece.piece == "P") {
            //Promote!
            promoteQueen(piece);
            return;
        }
    }
}

function promoteQueen(piece) {
    piece.piece = "Q";
    alert("Promote!");
}

function checkWin(board) {
    var whiteKing = false;
    var blackKing = false;

    for(var row = 1; row < 9; row++) {
        for (var col = 1; col < 9; col++) {
            var piece = board[row][col];

            if (piece.piece === "K") {

                //Its king!
                if (piece.owner == 1) {
                    whiteKing = true;
                } else {
                    blackKing = true;
                }
            }
        }
    }

    //Check if both kings are in play
    if (whiteKing && blackKing) {
        return false;
    }

    if (whiteKing) {
        //White wins
        alert("White Wins!");
        return true;
    } else {
        //Black wins
        alert("Black wins!");
        return true;
    }
}

export default PieceDetection;
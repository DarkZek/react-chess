import GameTile from './Chess.js'

function DetectMoves(row, column, piece, board) {
    
    switch(piece) {
        case "P": {
            return PawnMovement(row, column, board);
        }
        case "R": {
            return RookMovement(row, column, board);
        }
        case "B": {
            return BishopMovement(row, column, board);
        }
        case "Q": {
            return QueenMovement(row, column, board);
        }
        case "K": {
            return KingMovement(row, column, board);
        }
        case "k": {
            return KnightMovement(row, column, board);
        }
    }

    return [];
}

function PawnMovement(row, column, board) {
    var points = [];
    
    if (row == 1) {
        return points;
    }

    if (board[row + 1][column].piece == "") {
        points.push(row + 1 + "" + column);
    }

    //Check bottom left
    if (column > 1 && board[row + 1][column - 1].owner == 1) {
        points.push((row + 1) + "" + (column - 1));
    }

    //Check bottom right
    if (column < 8 && board[row + 1][column + 1].owner == 1) {
        points.push((row + 1) + "" + (column + 1));
    }

    if (row == 7) {
        points.push(row - 2 + "" + column);
    }
    
    return points;
}

function RookMovement(row, column, board) {
    var spots = [];

    var pieceX = row;
    var pieceY = column;

    //Find all spots to the left of rook
    for(var y = pieceY - 1; y > 0; y--) {

        //Check if piece there
        if (board[pieceX][y].owner == 2) {
            break;
        }

        spots.push(pieceX + "" + y);

        if (board[pieceX][y].owner == 1) {
            break;
        }  
    }

    //Find all spots to the top of rook
    for(var x = pieceX - 1; x > 0; x--) {

        //Check if piece there
        if (board[x][pieceY].owner == 2) {
            break;
        }

        spots.push(x + "" + pieceY);

        if (board[x][pieceY].owner == 1) {
            break;
        }  
    }

    //Find all spots to the right of rook
    for(var y = pieceY + 1; y < 9; y++) {

        //Check if piece there
        if (board[pieceX][y].owner == 2) {
            break;
        }

        spots.push(pieceX + "" + y);

        if (board[pieceX][y].owner == 1) {
            break;
        }  
    }

    //Find all spots to the bottom of rook
    for(var x = pieceX + 1; x < 9; x++) {

        //Check if piece there
        if (board[x][pieceY].owner == 2) {
            break;
        }

        spots.push(x + "" + pieceY);

        if (board[x][pieceY].owner == 1) {
            break;
        }  
    }

    return spots;
}

function KnightMovement(row, column, board) {
    var spots = [];

    // Top top right
    if (row - 2 > 0 && column + 1 < 9) {
        if (board[row - 2][column + 1].owner != 2) {
            spots.push((row - 2) + "" + (column + 1));
        }
    }

    // Top right right
    if (row - 1 > 0 && column + 2 < 9) {
        if (board[row - 1][column + 2].owner != 2) {
            spots.push((row - 1) + "" + (column + 2));
        }
    }

    // bottom right right
    if (row + 1 < 9 && column + 2 < 9) {
        if (board[row + 1][column + 2].owner != 2) {
            spots.push((row + 1) + "" + (column + 2));
        }
    }

    // bottom bottom right
    if (row + 2 < 9 && column + 1 < 9) {
        if (board[row + 2][column + 1].owner != 2) {
            spots.push((row + 2) + "" + (column + 1));
        }
    }

    // bottom bottom left
    if (row + 2 < 9 && column - 1 > 0) {
        if (board[row + 2][column - 1].owner != 2) {
            spots.push((row + 2) + "" + (column - 1));
        }
    }

    // bottom left left
    if (row + 1 < 9 && column - 2 > 0) {
        if (board[row + 1][column - 2].owner != 2) {
            spots.push((row + 1) + "" + (column - 2));
        }
    }

    // top left left
    if (row - 1 > 0 && column - 2 > 0) {
        if (board[row - 1][column - 2].owner != 2) {
            spots.push((row - 1) + "" + (column - 2));
        }
    }

    // top top left
    if (row - 2 > 0 && column - 1 > 0) {
        if (board[row - 2][column - 1].owner != 2) {
            spots.push((row - 2) + "" + (column - 1));
        }
    }

    return spots;
}

function QueenMovement(row, column, board) {
    var spots = [];

    var pieceX = row;
    var pieceY = column;

    //Find all spots to the bottom-right of queen
    for(var distance = 1; distance < 9; distance++) {
        var x = (pieceX + distance);
        var y = (pieceY + distance);

        if (x > 8 || x < 1 || y > 8 || y < 1) {
            break;
        }

        //Check if piece there
        if (board[x][y].owner == 2) {
            break;
        }

        spots.push(x + "" + y);

        if (board[x][y].owner == 1) {
            break;
        }
    }

    //Find all spots to the bottom-left of queen
    for(var distance = 1; distance < 9; distance++) {
        var x = (pieceX + distance);
        var y = (pieceY - distance);

        if (x > 8 || x < 1 || y > 8 || y < 1) {
            break;
        }

        //Check if piece there
        if (board[x][y].owner == 2) {
            break;
        }

        spots.push(x + "" + y);

        if (board[x][y].owner == 1) {
            break;
        }
    }

    //Find all spots to the top-right of queen
    for(var distance = 1; distance < 9; distance++) {
        var x = (pieceX - distance);
        var y = (pieceY + distance);

        if (x > 8 || x < 1 || y > 8 || y < 1) {
            break;
        }

        //Check if piece there
        if (board[x][y].owner == 2) {
            break;
        }

        spots.push(x + "" + y);

        if (board[x][y].owner == 1) {
            break;
        }
    }

    //Find all spots to the top-left of queen
    for(var distance = 1; distance < 9; distance++) {
        var x = (pieceX - distance);
        var y = (pieceY - distance);

        if (x > 8 || x < 1 || y > 8 || y < 1) {
            break;
        }

        //Check if piece there
        if (board[x][y].owner == 2) {
            break;
        }

        spots.push(x + "" + y);

        if (board[x][y].owner == 1) {
            break;
        }
    }

    //Find all spots to the left of queen
    for(var y = pieceY - 1; y > 0; y--) {

        //Check if piece there
        if (board[pieceX][y].owner == 2) {
            break;
        }

        spots.push(pieceX + "" + y);

        if (board[pieceX][y].owner == 1) {
            break;
        }  
    }

    //Find all spots to the top of queen
    for(var x = pieceX - 1; x > 0; x--) {

        //Check if piece there
        if (board[x][pieceY].owner == 2) {
            break;
        }

        spots.push(x + "" + pieceY);

        if (board[x][pieceY].owner == 1) {
            break;
        }  
    }

    //Find all spots to the right of queen
    for(var y = pieceY + 1; y < 9; y++) {

        //Check if piece there
        if (board[pieceX][y].owner == 2) {
            break;
        }

        spots.push(pieceX + "" + y);

        if (board[pieceX][y].owner == 1) {
            break;
        }  
    }

    //Find all spots to the bottom of queen
    for(var x = pieceX + 1; x < 9; x++) {

        //Check if piece there
        if (board[x][pieceY].owner == 2) {
            break;
        }

        spots.push(x + "" + pieceY);

        if (board[x][pieceY].owner == 1) {
            break;
        }  
    }

    return spots;
}

function BishopMovement(row, column, board) {
    var spots = [];

    var pieceX = row;
    var pieceY = column;

    //Find all spots to the bottom-right of bishop
    for(var distance = 1; distance < 9; distance++) {
        var x = (pieceX + distance);
        var y = (pieceY + distance);

        if (x > 8 || x < 1 || y > 8 || y < 1) {
            break;
        }

        //Check if piece there
        if (board[x][y].owner == 2) {
            break;
        }

        spots.push(x + "" + y);

        if (board[x][y].owner == 1) {
            break;
        }
    }

    //Find all spots to the bottom-left of bishop
    for(var distance = 1; distance < 9; distance++) {
        var x = (pieceX + distance);
        var y = (pieceY - distance);

        if (x > 8 || x < 1 || y > 8 || y < 1) {
            break;
        }

        //Check if piece there
        if (board[x][y].owner == 2) {
            break;
        }

        spots.push(x + "" + y);

        if (board[x][y].owner == 1) {
            break;
        }
    }

    //Find all spots to the top-right of bishop
    for(var distance = 1; distance < 9; distance++) {
        var x = (pieceX - distance);
        var y = (pieceY + distance);

        if (x > 8 || x < 1 || y > 8 || y < 1) {
            break;
        }

        //Check if piece there
        if (board[x][y].owner == 2) {
            break;
        }

        spots.push(x + "" + y);

        if (board[x][y].owner == 1) {
            break;
        }
    }

    //Find all spots to the top-left of bishop
    for(var distance = 1; distance < 9; distance++) {
        var x = (pieceX - distance);
        var y = (pieceY - distance);

        if (x > 8 || x < 1 || y > 8 || y < 1) {
            break;
        }

        //Check if piece there
        if (board[x][y].owner == 2) {
            break;
        }

        spots.push(x + "" + y);

        if (board[x][y].owner == 1) {
            break;
        }
    }

    return spots;
}

function KingMovement(row, column, board) {
    var spots = [];
    
    //Bottom spot
    if (row < 8 && board[row + 1][(column)].owner != 2) {
        spots.push((row + 1) + "" + column);
    }

    //Bottom left spot
    if (row < 8 && column > 1 && board[row + 1][(column - 1)].owner != 2) {
        spots.push((row + 1) + "" + (column - 1));
    }

    //left spot
    if (column > 1 && board[row][(column - 1)].owner != 2) {
        spots.push(row + "" + (column - 1));
    }
    
    //left top spot
    if (row > 1 && column > 1 && board[row - 1][(column - 1)].owner != 2) {
        spots.push((row - 1) + "" + (column - 1));
    }

    //top spot
    if (row > 1 && board[row - 1][(column)].owner != 2) {
        spots.push((row - 1) + "" + column);
    }

    //top right spot
    if (row > 1 && column < 8 && board[row - 1][(column + 1)].owner != 2) {
        spots.push((row - 1) + "" + (column + 1));
    }

    //right spot
    if (column < 8 && board[row][(column + 1)].owner != 2) {
        spots.push(row + "" + (column + 1));
    }

    //right spot
    if (row < 8 && column < 8 && board[row + 1][(column + 1)].owner != 2) {
        spots.push((row + 1) + "" + (column + 1));
    }

    return spots;
}

export default DetectMoves;